'use strict'

var TR = require('./tr.js');

require('./theme.less');

module.exports = class {
  constructor(items_in_row, width, padding, min_a, element, auto_width) {
    this.element = element || document.createElement('table');
    this.element.classList.add('grid_ui');

    this.element.cellSpacing = 0;
    this.element.cellPadding = 0;

    for (let i = 0; i < items_in_row; i++) {
      this.element.appendChild(document.createElement('col'));
    }

    this.padding = padding;
    this.min_a = min_a;
    this.min_width = items_in_row*min_a+padding;
  //  this.element.style.minWidth = this.min_width+"px";
    this.items_in_row = items_in_row;
    this.inrow_default = items_in_row;
    this.item_a = width/items_in_row-padding;
    if (this.item_a < this.min_a) {
      this.item_a = this.min_a;
    }

    this.trs = [];
    this.tds = [];
    this.cur_tr = new TR(items_in_row);
    this.element.appendChild(this.cur_tr.element);
    this.trs.push(this.cur_tr);

    this.auto_width = auto_width;
  }

  reconstruct() {
    this.trs = [];
    let cloned_tds = [];
    for (var i = 0; i < this.tds.length; i++) {
      cloned_tds.push(this.tds[i]);
    }
    this.tds = [];

    this.element.innerHTML = "";
    this.cur_tr = new TR(this.items_in_row);
    this.element.appendChild(this.cur_tr.element);
    this.trs.push(this.cur_tr);

    for (var i = 0; i < cloned_tds.length; i++) {
      if (this.cur_tr.items == this.items_in_row) {
        this.cur_tr = new TR(this.items_in_row);
        this.element.appendChild(this.cur_tr.element);
        this.trs.push(this.cur_tr);

        this.cur_tr.add(cloned_tds[i]);
        this.tds.push(cloned_tds[i]);
      } else {
        this.cur_tr.add(cloned_tds[i]);
        this.tds.push(cloned_tds[i]);
      }
    }
  }

  resize(width) {


    if (width) {
      this.cur_width = width;
      width -= getScrollbarWidth();
      const width_per_item = width/this.inrow_default-this.padding;
      if (width_per_item < this.min_a) {
        this.items_in_row = Math.floor((width-this.padding*this.inrow_default)/this.min_a);
      } else {
        this.items_in_row = this.inrow_default;
      }
      if (this.items_in_row < 1) this.items_in_row = 1;
      this.reconstruct();

      this.item_a = Math.floor(width/this.items_in_row-this.padding*2);
      if (this.item_a < this.min_a) {
        this.item_a = this.min_a;
      }
    }

    if (!this.auto_width) {
      for (var i = 0; i < this.tds.length; i++) {
        this.tds[i].style.width = this.item_a+"px";
        this.tds[i].style.height = this.item_a+"px";
        this.tds[i].firstChild.style.width = this.item_a+"px";
        this.tds[i].firstChild.style.height = this.item_a+"px";

        this.tds[i].style.minWidth = this.min_a+"px";
        this.tds[i].style.mimHeight = this.min_a+"px";
      }

      this.element.style.width = (this.item_a+this.padding)*this.items_in_row+"px";

      this.element.parentNode.style.minHeight = this.element.offsetHeight+"px";
    }

  }

  add(item) {
    if (this.cur_tr.items == this.cur_tr.max_items) {
      this.cur_tr = new TR(this.items_in_row);
      this.element.appendChild(this.cur_tr.element);

      var td = document.createElement('td');
      td.appendChild(item);
      this.cur_tr.add(td);

      this.trs.push(this.cur_tr);
      this.tds.push(td);
      this.resize(this.cur_width);
    } else {
      var td = document.createElement('td');
      td.appendChild(item);
      this.cur_tr.add(td);
      this.tds.push(td);
      this.resize(this.cur_width);
    }
  }

  insert(item, index) {
    

    if (index >= this.tds.length) {
      console.log("RE ADD");
      this.add(item);
    } else {
      let cur_tr = this.trs[Math.floor(index / this.items_in_row)];
      
      let td = document.createElement('td');
      td.appendChild(item);

      cur_tr.insert(td, index % this.items_in_row);

      this.tds.splice(index, 0, td);
      this.resize(this.cur_width);
    }
  }

  remove(item) {
    const td_index = this.tds.indexOf(item.parentNode);
    this.tds.splice(td_index, 1)
    for (var t = 0; t < this.trs.length; t++) {
      var tr = this.trs[t];
      if (tr.contains(item)) {
        tr.remove(item.parentNode);
        if (tr.items == 0 && t > 0) {
          this.element.removeChild(tr.element);
          this.trs.splice(t, 1);
          this.cur_tr = this.trs[t-1];
        } else if (tr.items == tr.max_items-1) {
          this.track(t+1);
        }
      }
    }
  }

  remove_all() {
    this.element.innerHTML = "";
    this.tds = [];
    this.trs = [];
  }

  track(t) {
    if (t < this.trs.length) {
      var tr = this.trs[t];
      if (tr.items > 0) {
        var td = tr.tds[0];
        tr.remove(td);
        this.trs[t-1].add(td);
        if (tr.items == tr.max_items-1) {
          this.track(t+1);
        }
      } else {
        this.element.removeChild(this.trs[t].element);
        this.trs.splice(t, 1);
        this.cur_tr = this.trs[t-1];
      }
    }
  }
}

function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}
